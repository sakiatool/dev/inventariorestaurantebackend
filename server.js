import express from 'express';
import cors from 'cors';
import bcrypt from 'bcrypt';
import knex from 'knex';
import dotenv from "dotenv/config";

import Register from './controllers/Register.js';
import SignIn from './controllers/SignIn.js';
import { AddSucursal, ImportSucursales } from './controllers/ManageSucursales.js';
import { ImportIngredientes, AddIngredient, DecreaseIngredientAmount, RemoveIngredient, UpgradeIngredientInfo } from './controllers/ManageIngredient.js';
import { AddRecipe, DecreaseRecipeAmount, ImportRecipes, UpdateRecipe } from './controllers/ManageRecipes.js';
import { AddDish, fetchDishes, UpdateDishes } from './controllers/ManagePlatos.js';
import { ImportingXLSX, ReadFile, WriteFile } from './controllers/Excel.js';

import multer from 'multer';

//Server
const server = express();
const PORT = process.env.PORT ?? 4000;
const upload = multer({dest: 'uploads/'});
server.listen(PORT, () =>{
    console.log(`App is running on port ${PORT}`);
});

//CORS and Middleware
server.use(cors());
server.use(express.json());

//DATABASE using PostgreSQL
const database = knex({
    client: 'pg',
    connection: {
      host : process.env.PGHOST ?? '127.0.0.1',
      port : process.env.PGPORT ?? 5432,
      user : process.env.PGUSER ?? 'postgres',
      password : process.env.PGPASSWORD ?? 'admin',
      database : process.env.PGDATABASE ?? 'postgres'
    }
});

var unidades = 
{
    'Longitud': 
    {
        'grupo': 'Longitud',
        'unidades': 
            [
                'kilometro',
                'metro',
                'centimetro',
                'milimetro',
                'micrometro',
                'nanometro',
                'milla',
                'yarda',
                'pie',
                'pulgada'
            ],
        'equivalencias': 
            {
                'kilometro': 
                {
                    'kilometro': 1,
                    'metro': 1000,
                    'centimetro': 100000,
                    'milimetro': 1e+6,
                    'micrometro': 1e+9,
                    'nanometro': 1e+12,
                    'milla': 0.621371,
                    'yarda': 1093.61,
                    'pie': 3280.84,
                    'pulgada': 39370.1
                },
            }    
    },
    'Peso': 
    {
        'grupo': 'Peso',
        'unidades': 
            [
                'Libras',
                'Kilogramos',
                'Onzas',
                'Piedras',
                'Libras de Troy',
                'Onzas de Troy',
                'Gramos',
                'Miligramos',
                'Microgramos',
                'Quilates',
                'Quintales cortos',
                'Quintales largos'
            ],
        'equivalencias': 
            {
                'Gramos': 
                {
                    'Libras' : 1/0.0022046,
                    'Kilogramos' : 1000,
                    'Onzas' : 1/0.035274,
                    'Piedras' : 1/0.00015747,
                    'Libras de Troy' : 1/0.0026792,
                    'Onzas de Troy' : 1/0.032151,
                    'Gramos' : 1,
                    'Miligramos' : 1/1000,
                    'Microgramos' : 1/1000000,
                    'Quilates' : 1/5,
                    'Quintales cortos' : 1/0.000022046,
                    'Quintales largos' : 1/0.000019684
                },
            }    
    },
    'Volumen': 
    {
        'grupo': 'Volumen',
        'unidades': 
            [
                'Litros',
                'Mililitros',
                'Cuartos de Galón',
                'Cucharilla de café',
                'Barriles',
                'Cucharadas',
                'Cucharillas',
                'Tazas',
                'Galones',
                'Pintas',
                'Onzas',
                'Cucharillas',
                'Tazas',
                'Galones' 
            ],
        'equivalencias': 
            {
                'Mililitros': 
                {
                    'Litros' : 1000,
                    'Mililitros' : 1,
                    'Cuartos de Galón' : 1/0.0010567,
                    'Cucharilla métrica' : 1/0.2,
                    'Barriles' : 1/0.0000083864,
                    'Cucharadas' : 1/0.067628,
                    'Cucharillas' : 1/0.20288,
                    'Tazas' : 1/0.0042268,
                    'Tazas métricas' : 1/0.004,
                    'Galones' : 3785.4118,
                    'Pintas' : 1/0.0021134,
                    'Onzas' : 1/0.033814
                },
            }    
    }
};

class Conversion
{
    constructor()
    {
    }

    Convert(from,to,qty,Type,Revert = false)
    {
        if(Revert)
        {
            return qty / unidades[Type]["equivalencias"][from][to];
        }else
        {
            return qty * unidades[Type]["equivalencias"][to][from];
        }
    }

    ToGrames(Type,Qty)
    {
        return this.Convert(Type,"Gramos",Qty,"Peso",false);
    }

    FromGrames(Type,Qty)
    {
        return this.Convert("Gramos",Type,Qty,"Peso",true);
    }

    ToMililitres(Type,Qty)
    {
        return this.Convert(Type,"Mililitros",Qty,"Volumen",false);
    }

    FromMililitres(Type,Qty)
    {
        return this.Convert("Mililitros",Type,Qty,"Volumen",true);
    }
}

var calculator = new Conversion();



// Server startup
server.get('/', (req,res) => res.json("App is running properly"));

// Pruebas DB
server.get('/empresas', (req,res) => {
  
  const { id, token } = req.query;
  

  const query = database.from('inventario_db.data_users_view').select("*").where({ id_usertable: parseInt(id) });

  query.then((results) => {

    res.json(results);
  });

  
});

server.get('/ingredientes', (req,res) => {
  
  const { id, token,sucursal } = req.query;
  

  const query = database.from('inventario_db.ingredientes_sucursales').select("*").where({ sucursal: sucursal });
  console.log(sucursal);

  query.then((results) => {

    res.json(results);
  });

  
});

server.get('/recetas', (req,res) => {
  
  const { id, token,sucursal } = req.query;
  

  const query = database.from('inventario_db.recetas_sucursales').select("*").where({ sucursal: sucursal });
  console.log(sucursal);

  query.then((results) => {

    res.json(results);
  });

  
});

server.get('/platos', (req,res) => {
  
  const { id, token,sucursal } = req.query;
  

  const query = database.from('inventario_db.platos_sucursales').select("*").where({ sucursal: sucursal });
  console.log(sucursal);

  query.then((results) => {

    res.json(results);
  });

  
});

server.get('/ventas', (req,res) => {
  
  const { id, token,sucursal } = req.query;
  

  const query = database.from('inventario_db.ventas_sucursales').select("*").where({ sucursal: sucursal });
  console.log(sucursal);

  query.then((results) => {

    res.json(results);
  });

  
});

server.post('/agregarventas', async (req, res) => {
  const { id, token, sucursal, nombre, cantidad, disponible, platos_id, fecha} = req.query;

  try {
    // Consultar la tabla de sucursales para obtener el ID de la sucursal
    const results = await database.from('inventario_db.sucursales').select("id").where({ nombre: sucursal });
    console.log(sucursal);

    if (results.length === 0) {
      // Si no se encontró la sucursal, devolver un error
      return res.status(404).json({ error: 'La sucursal no existe' });
    }

    // Datos del nuevo registro a insertar
    const nuevoRegistro = {
      nombre: nombre,
      cantidad: cantidad,
      disponible: disponible,
      platos_id: platos_id,
      sucursales_id: results[0].id,
      fecha: fecha,
      // Agrega más campos y valores según tu tabla
    };

    // Realizar la inserción del nuevo registro en la tabla de ventas
    const resultado = await database.from('inventario_db.ventas').insert(nuevoRegistro);

    // Devolver la respuesta al cliente
    res.json({ mensaje: 'Registro insertado correctamente', resultado: resultado });
  } catch (error) {
    // Manejar errores en caso de que ocurra alguno durante la consulta o inserción
    console.error('Error al agregar ventas:', error);
    res.status(500).json({ error: 'Error interno del servidor' });
  }
});

server.delete('/eliminarventas', async (req, res) => {
  const { id, token, sucursal,id_ventas } = req.query;

  try {
    // Realizar la eliminación del registro en la base de datos
    const deletedRows = await database
      .from('inventario_db.ventas')
      .where({ id: id_ventas })
      .del();

    // Verificar si se eliminaron registros y enviar una respuesta apropiada
    if (deletedRows > 0) {
      res.status(200).json({ message: 'Registro eliminado correctamente' });
    } else {
      res.status(404).json({ message: 'Registro no encontrado' });
    }
  } catch (error) {
    console.error('Error al eliminar el registro:', error);
    res.status(500).json({ message: 'Error al eliminar el registro' });
  }
});

server.post('/agregaringredientes', async (req, res) => {
  const { id, token, sucursal, nombre, disponible, costo,cantidad,unidad} = req.query;

  try {
    // Consultar la tabla de sucursales para obtener el ID de la sucursal
    const results = await database.from('inventario_db.sucursales').select("id").where({ nombre: sucursal });
    console.log(sucursal);

    if (results.length === 0) {
      // Si no se encontró la sucursal, devolver un error
      return res.status(404).json({ error: 'La sucursal no existe' });
    }

    let cantidad2 = parseInt(cantidad);

    let condiciones = ["Libras", "Kilogramos", "Onzas", "Piedras", "Libras de Troy", "Onzas de Troy", "Gramos", "Miligramos", "Microgramos", "Quilates", "Quintales cortos", "Quintales largos"];
    let condiciones_volumen = ["Litros", "Mililitros", "Cuartos de Galón", "Cucharilla de café", "Barriles", "Cucharadas", "Cucharillas", "Tazas", "Pintas", "Cucharillas", "Tazas métricas", "Galones"];
    let condicion_unidad = ["Unidad",""];
    if (condiciones.includes(unidad)){
      let cantidad_gramos = calculator.ToGrames(unidad,cantidad2);
      var costo_cantidad_unidad_gramos = costo/cantidad_gramos;

    }else if (condiciones_volumen.includes(unidad)){
      // aqui asumimos que 1 mL = a 1 gramo
      let cantidad_mililitros = calculator.ToMililitres(unidad,cantidad2);
      var costo_cantidad_unidad_gramos = costo/cantidad_mililitros;

    }else if (condicion_unidad.includes(unidad)){
      
      var costo_cantidad_unidad_gramos = costo/canticantidad2dad;
    }

    // Datos del nuevo registro a insertar
    const nuevoRegistro = {
      nombre: nombre,
      disponible: disponible,
      costo: costo,
      sucursales_id: results[0].id,
      cantidad: cantidad,
      unidad: unidad,
      costo_unidad_gramo: costo_cantidad_unidad_gramos
      // Agrega más campos y valores según tu tabla
    };

    console.log(nuevoRegistro);

    // Realizar la inserción del nuevo registro en la tabla de ventas
    const resultado = await database.from('inventario_db.ingredientes').insert(nuevoRegistro);

    // Devolver la respuesta al cliente
    res.json({ mensaje: 'Registro insertado correctamente', resultado: resultado });
  } catch (error) {
    // Manejar errores en caso de que ocurra alguno durante la consulta o inserción
    console.error('Error al agregar ventas:', error);
    res.status(500).json({ error: 'Error interno del servidor' });
  }
});

server.delete('/eliminaringredientes', async (req, res) => {
  const { id, token, sucursal,id_ingrediente } = req.query;

  try {
    // Realizar la eliminación del registro en la base de datos
    const deletedRows = await database
      .from('inventario_db.ingredientes')
      .where({ id: id_ingrediente })
      .del();

    // Verificar si se eliminaron registros y enviar una respuesta apropiada
    if (deletedRows > 0) {
      res.status(200).json({ message: 'Registro eliminado correctamente' });
    } else {
      res.status(404).json({ message: 'Registro no encontrado' });
    }
  } catch (error) {
    console.error('Error al eliminar el registro:', error);
    res.status(500).json({ message: 'Error al eliminar el registro' });
  }
});

server.post('/agregarrecetas', async (req, res) => {
  const { id, token, sucursal, nombre, disponible,cantidad,unidad, ingredientes} = req.query;

  try {
    // Consultar la tabla de sucursales para obtener el ID de la sucursal
    const results = await database.from('inventario_db.sucursales').select("id").where({ nombre: sucursal });
    console.log(sucursal);

    if (results.length === 0) {
      // Si no se encontró la sucursal, devolver un error
      return res.status(404).json({ error: 'La sucursal no existe' });
    }
    console.log(req.query);
    let ingredientesData = JSON.parse(ingredientes);

    let condiciones = ["Libras", "Kilogramos", "Onzas", "Piedras", "Libras de Troy", "Onzas de Troy", "Gramos", "Miligramos", "Microgramos", "Quilates", "Quintales cortos", "Quintales largos"];
    let condiciones_volumen = ["Litros", "Mililitros", "Cuartos de Galón", "Cucharilla de café", "Barriles", "Cucharadas", "Cucharillas", "Tazas", "Pintas", "Cucharillas", "Tazas métricas", "Galones"];
    let condicion_unidad = ["Unidad",""];
    var unidad2 = unidad;
    var cantidad2 = parseInt(cantidad);
    if(unidad2===""){
      unidad2="Unidad"
    }

    if (condiciones.includes(unidad)){
      console.log("prueba1");
      var cantidad_gramos = calculator.ToGrames(unidad,cantidad2);
            

    }else if (condiciones_volumen.includes(unidad)){
      console.log("prueba2");
            // aqui asumimos que 1 mL = a 1 gramo
      var cantidad_gramos = calculator.ToMililitres(unidad,cantidad2);
            
    }else if (condicion_unidad.includes(unidad)){

      var cantidad_gramos = cantidad2;
    }

    
    // Datos del nuevo registro a insertar
    const nuevoRegistro = {
      nombre: nombre,
      disponible: disponible,
      sucursales_id: results[0].id,
      cantidad:cantidad,
      unidad:unidad2,
      cantidad_gramos:cantidad_gramos,
      // Agrega más campos y valores según tu tabla
    };

    console.log(nuevoRegistro);
    const resultado = await database.from('inventario_db.recetas').insert(nuevoRegistro);

    try {
      // Consultar la tabla de sucursales para obtener el ID de la sucursal
      const results2 = await database.from('inventario_db.recetas').select("id").where({ nombre: nombre });

      


      
  
      if (results2.length === 0) {
        // Si no se encontró la sucursal, devolver un error
        return res.status(404).json({ error: 'La sucursal no existe' });
      }else {

        console.log(results2[0].id);
        for (const [key, value] of Object.entries(ingredientesData)) {

      
          console.log(key);
          console.log(value);


          if (condiciones.includes(value[2])){
            console.log("prueba1");
            var cantidad_gramos = calculator.ToGrames(value[2],value[1]);
            

          }else if (condiciones_volumen.includes(value[2])){
            console.log("prueba2");
            // aqui asumimos que 1 mL = a 1 gramo
            var cantidad_gramos = calculator.ToMililitres(value[2],value[1]);
            
          }else if (condicion_unidad.includes(value[2])){
            console.log("aqui");
            console.log(value[1]);
            var cantidad_gramos = value[1];
          }

 
          const nuevoRegistroPivotTable = {
            id_recetas:results2[0].id,
            id_ingredientes:value[0],
            id_sucursales:results[0].id,
            cantidad_ingrediente:value[1],
            unidad_ingrediente:value[2],
            cantidad_gramos_ingredientes:cantidad_gramos,


            // Agrega más campos y valores según tu tabla
          };

          console.log(nuevoRegistroPivotTable);
          const resultado2 = await database.from('inventario_db.recetas_ingredientes').insert(nuevoRegistroPivotTable);


        }
      }
    
    }catch (e){
        console.log(e);
      }

    

    

    

    // Devolver la respuesta al cliente
    res.json({ mensaje: 'Registro insertado correctamente', resultado: nuevoRegistro });
  } catch (error) {
    // Manejar errores en caso de que ocurra alguno durante la consulta o inserción
    console.error('Error al agregar ventas:', error);
    res.status(500).json({ error: 'Error interno del servidor' });
  }
});

server.delete('/eliminarrecetas', async (req, res) => {
  const { id, token, sucursal,id_recetas } = req.query;

  try {
    // Realizar la eliminación del registro en la base de datos
    const deletedRows = await database
      .from('inventario_db.recetas_ingredientes')
      .where({ id_recetas: id_recetas })
      .del();

    const deletedRows2 = await database
      .from('inventario_db.recetas')
      .where({ id: id_recetas })
      .del();

    // Verificar si se eliminaron registros y enviar una respuesta apropiada
    if (deletedRows > 0) {
      res.status(200).json({ message: 'Registro eliminado correctamente' });

      
    } else {
      res.status(404).json({ message: 'Registro no encontrado' });
    }
  } catch (error) {
    console.error('Error al eliminar el registro:', error);
    res.status(500).json({ message: 'Error al eliminar el registro' });
  }


});

server.post('/agregarplatos', async (req, res) => {
  const { id, token, sucursal, nombre,precio,disponible, ingredientes,recetas} = req.query;

  try {
    // Consultar la tabla de sucursales para obtener el ID de la sucursal
    const results = await database.from('inventario_db.sucursales').select("id").where({ nombre: sucursal });
    console.log(sucursal);

    if (results.length === 0) {
      // Si no se encontró la sucursal, devolver un error
      return res.status(404).json({ error: 'La sucursal no existe' });
    }
    console.log(req.query);
    let ingredientesData = JSON.parse(ingredientes);
    let recetasData = JSON.parse(recetas);


    // Datos del nuevo registro a insertar
    const nuevoRegistro = {
      nombre: nombre,
      precio_venta:precio,
      disponible: disponible,
      sucursales_id: results[0].id,
      // Agrega más campos y valores según tu tabla
    };

    console.log(nuevoRegistro);
    const resultado = await database.from('inventario_db.platos').insert(nuevoRegistro);

    try {
      // Consultar la tabla de sucursales para obtener el ID de la sucursal
      const results2 = await database.from('inventario_db.platos').select("id").where({ nombre: nombre });

  
      if (results2.length === 0) {
        // Si no se encontró la sucursal, devolver un error
        return res.status(404).json({ error: 'La sucursal no existe' });
      }else {

        console.log(results2[0].id);

        
        for (const [key, value] of Object.entries(ingredientesData)) {

          let cantidad2 = parseInt(value[1]);

          let condiciones = ["Libras", "Kilogramos", "Onzas", "Piedras", "Libras de Troy", "Onzas de Troy", "Gramos", "Miligramos", "Microgramos", "Quilates", "Quintales cortos", "Quintales largos"];
          let condiciones_volumen = ["Litros", "Mililitros", "Cuartos de Galón", "Cucharilla de café", "Barriles", "Cucharadas", "Cucharillas", "Tazas", "Pintas", "Cucharillas", "Tazas métricas", "Galones"];
          let condicion_unidad = ["Unidad",""];

          if (condiciones.includes(value[2])){
            var cantidad_gramos = calculator.ToGrames(value[2],cantidad2);
            

          }else if (condiciones_volumen.includes(value[2])){
            // aqui asumimos que 1 mL = a 1 gramo
            var cantidad_gramos = calculator.ToMililitres(value[2],cantidad2);
            

          }else if (condicion_unidad.includes(value[2])){
            
            var cantidad_gramos = cantidad2;
          }

      
          console.log(key);
          console.log(value);

          const nuevoRegistroPivotTable = {
            id_platos:results2[0].id,
            id_ingredientes:value[0],
            id_sucursales:results[0].id,
            cantidad_ingredientes:value[1],
            unidad_ingredientes:value[2],
            cantidad_gramos_ingredientes:cantidad_gramos,
            // Agrega más campos y valores según tu tabla
          };

          console.log(nuevoRegistroPivotTable);
          const resultado2 = await database.from('inventario_db.platos_ingredientes').insert(nuevoRegistroPivotTable);
        }


          for (const [key, value] of Object.entries(recetasData)) {


            let cantidad2 = parseInt(value[1]);

            let condiciones = ["Libras", "Kilogramos", "Onzas", "Piedras", "Libras de Troy", "Onzas de Troy", "Gramos", "Miligramos", "Microgramos", "Quilates", "Quintales cortos", "Quintales largos"];
            let condiciones_volumen = ["Litros", "Mililitros", "Cuartos de Galón", "Cucharilla de café", "Barriles", "Cucharadas", "Cucharillas", "Tazas", "Pintas", "Cucharillas", "Tazas métricas", "Galones"];
            let condicion_unidad = ["Unidad",""];

            if (condiciones.includes(value[2])){
              var cantidad_gramos = calculator.ToGrames(value[2],cantidad2);
              

            }else if (condiciones_volumen.includes(value[2])){
              // aqui asumimos que 1 mL = a 1 gramo
              var cantidad_gramos = calculator.ToMililitres(value[2],cantidad2);
              

            }else if (condicion_unidad.includes(value[2])){
              
              var cantidad_gramos = cantidad2;
            }

      
            console.log(key);
            console.log(value);
  
            const nuevoRegistroPivotTable2 = {
              id_platos:results2[0].id,
              id_recetas:value[0],
              id_sucursales:results[0].id,
              cantidad_recetas:value[1],
              unidad_recetas:value[2],
              cantidad_gramos_recetas:cantidad_gramos,
              // Agrega más campos y valores según tu tabla
            };
  
            console.log(nuevoRegistroPivotTable2);
            const resultado3 = await database.from('inventario_db.platos_recetas').insert(nuevoRegistroPivotTable2);


        }
      }}catch (e){
        console.log(e);
      }

    

    

    

    // Devolver la respuesta al cliente
    res.json({ mensaje: 'Registro insertado correctamente', resultado: nuevoRegistro });
  } catch (error) {
    // Manejar errores en caso de que ocurra alguno durante la consulta o inserción
    console.error('Error al agregar ventas:', error);
    res.status(500).json({ error: 'Error interno del servidor' });
  }
});

server.delete('/eliminarplatos', async (req, res) => {
  const { id, token, sucursal,id_platos } = req.query;

  try {
    // Realizar la eliminación del registro en la base de datos
    const deletedRows = await database
      .from('inventario_db.platos_ingredientes')
      .where({ id_platos: id_platos })
      .del();

      const deletedRows3 = await database
      .from('inventario_db.platos_recetas')
      .where({ id_platos: id_platos })
      .del();

    const deletedRows2 = await database
      .from('inventario_db.platos')
      .where({ id: id_platos })
      .del();

    // Verificar si se eliminaron registros y enviar una respuesta apropiada
    if (deletedRows > 0) {
      res.status(200).json({ message: 'Registro eliminado correctamente' });

      
    } else {
      res.status(404).json({ message: 'Registro no encontrado' });
    }
  } catch (error) {
    console.error('Error al eliminar el registro:', error);
    res.status(500).json({ message: 'Error al eliminar el registro' });
  }


});

server.get('/productomasvendido', (req,res) => {
  
  const { id, token,sucursal,fechaI,fechaF } = req.query;

  console.log(fechaI);
  console.log(fechaF);

  

  const query = database('inventario_db.ventas_platos')
  .select('id_plato', 'plato').sum('cantidad_ventas')
  .where({
    sucursales_id: id
  }).whereBetween('fecha', [fechaI, fechaF]).groupBy('id_plato', 'plato').orderBy("sum", "desc").limit(10);

query.then((results) => {
    //console.log(results)

    res.json(results);
  });

  
});

server.get('/platocostoprecio', (req,res) => {
  
  const { id, token,sucursal} = req.query;

  

  const query = database('inventario_db.costo_precio_platos')
  .select("*")
  .where({
    sucursales_id: id
  });

query.then((results) => {
    //console.log(results)

    res.json(results);
  });

  
});

server.get('/adminpassword', (req,res) => {
  
  const { id, token,sucursal} = req.query;

  

  const query = database('inventario_db.costo_precio_platos')
  .select("*")
  .where({
    sucursales_id: id
  });

query.then((results) => {
    //console.log(results)

    res.json(results);
  });

  
});

/* CREDENTIALS */

//Put data to register a new user
server.put('/register', Register(database, bcrypt));
//Post data to log in
server.post('/signin', SignIn(database, bcrypt));

/* SUCURSALES */

//Post data from database/excel
server.post('/importsucursales', ImportSucursales(database));
//Post new sucursal to datbase
server.post('/addsucursal', AddSucursal(database));

/* INGREDIENTS*/ 

//Post data from database/excel
server.post('/importingredientes', ImportIngredientes(database));
//Put ingredients into database
server.put('/agregaringrediente', AddIngredient(database));
//Delete ingredients from database
server.delete('/removeingredient', RemoveIngredient(database));
//Decrease ingredientes in database
server.post('/decreaseingredient', DecreaseIngredientAmount(database));
// Update ingredients info on database
server.post('/updateingredients', UpgradeIngredientInfo(database));

/* RECIPES */

//Add recipe to database
server.put('/addrecipe', AddRecipe(database));
//Get recipes from database
server.post('/getrecipes', ImportRecipes(database));
//Decrease ingredientes in database
server.post('/decreaserecipe', DecreaseRecipeAmount(database));
//Update recipes on database
server.post('/updaterecipe', UpdateRecipe(database));

/* DISHES */

//Fetch dishes from database
server.post('/fetchdishes', fetchDishes(database))
//Add dish to database
server.put('/adddish', AddDish(database))
//Update dishes on database
server.post('/updatedishes', UpdateDishes(database));

/* EXCEL */

//Read excel uploaded from client
server.get('/readexcel', ReadFile);
//Write excel from database info
server.post('/writeexcel', WriteFile(database));
server.post('/ImportXLSX', upload.single('file'),WriteFile(database));
 

// server.get('/download', (req,res) => {
//     res.download('./elliott-wave-principle.pdf')
// })