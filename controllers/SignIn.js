const SignIn = (database, bcrypt) => (req, res) => {    
    const {email, password} = req.body;
    const saltRounds = 10;
    const hash = bcrypt.hashSync(password, saltRounds);
    //console.log(hash);



    let data = database.select('*').from('inventario_db.usuarios').where('email', email).then((rows) => {
        const isValid = bcrypt.compareSync(password, rows[0]["password"]);

        //console.log("estoy aqui");
        //console.log(isValid);

        if(isValid){
            rows[0]["token"] = 1;
        }
        else {
            rows[0]["token"] = 0;
        }
        
        res.json(rows);
      });

};

export default SignIn;